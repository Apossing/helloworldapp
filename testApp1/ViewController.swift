//
//  ViewController.swift
//  testApp1
//
//  Created by Adam on 12/26/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func displaymessage(_ sender: Any) {
        displayNotificationWithButton(message:"hello world")
        
    }
    
    @IBOutlet weak var TextBox1: UITextField!
    
    @IBAction func TextBox(_ sender: Any) {
        if (TextBox1.text == "")
        {
            TextBox1.text = "Hello World"
        }
        else
        {
            TextBox1.text = nil
        }
    }
    
    func displayNotification(message: String)
    {
        let alert = UIAlertController(title: "ALERT!", message: message, preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: {_ in alert.dismiss(animated: true, completion: nil)})
        
        
    }
    func displayNotificationWithButton(message: String)
    {
        let alert = UIAlertController(title: "FirstApp", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
}

